﻿// Lab05FileExp.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Lab05FileExp.h"
#include <windowsx.h>
#include <WinUser.h>
#include <minwinbase.h>
#include <CommCtrl.h>
#include <stdio.h>
#include <vector>
using namespace std;

#define MAX_LOADSTRING 100
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "Comctl32.lib")

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND listView;
HWND treeView;
HTREEITEM hRoot;
vector<TCHAR> logicalDriveList;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hwnd);
HWND CreateListView(HWND hWnd);
HWND CreateTreeView(HWND hWnd);
void InitFolderColumn(HWND hwndListView);
void InitDriveColumn(HWND hwndListView);
void LoadThisPC();
void ClearListView();
void GetDrive();
void LoadDriveForListView();
LPWSTR GetSize(__int64 size);
LPWSTR GetDateModified(const FILETIME &ftLastWrite);
void LoadForderContent(LPCWSTR path, HTREEITEM* hParent);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_LAB05FILEEXP, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LAB05FILEEXP));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LAB05FILEEXP));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_LAB05FILEEXP);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
	case WM_NOTIFY:
	{
		int wmId = LOWORD(wParam);

		WCHAR buffer[10240];
		WCHAR* temp;
		switch (wmId)
		{
			case IDC_LISTVIEW:
			{
				LPNMHDR info = (LPNMHDR)lParam;
				if (NM_DBLCLK == info->code)
				{
					int index = ListView_GetNextItem(listView, -1, LVNI_FOCUSED);

					LVITEM item;
					item.mask = LVIF_PARAM;
					item.iSubItem = 0;
					item.iItem = index;
					item.lParam = (LPARAM)buffer;

					ListView_GetItem(listView, &item);

					temp = (WCHAR*)item.lParam;

					ClearListView();
					InitFolderColumn(listView);
					LoadForderContent((WCHAR*)item.lParam, NULL);
				}
				break;
			}
		case IDC_TREEVIEW:
		{
			// A pointer to an NMHDR structure that contains
			// the notification code and additional information.
			// For some notification messages, 
			// this parameter points to a larger structure 
			// that has the NMHDR structure as its first member.

			LPNMTREEVIEW pnmtv = (LPNMTREEVIEW)lParam;
			NMHDR info = pnmtv->hdr;

			if (TVN_ITEMEXPANDING == info.code  || TVN_SELCHANGED == info.code)
			{
				if (TreeView_GetChild(treeView,TreeView_GetSelection(treeView)) == NULL)
				{
					if (TreeView_GetRoot(treeView) == TreeView_GetSelection(treeView))
					{
						GetDrive();
						ClearListView();
						InitDriveColumn(listView);
						LoadDriveForListView();
					}
					else
					{
						ClearListView();
						InitFolderColumn(listView);
						HTREEITEM hParent = TreeView_GetSelection(treeView);
						TVITEMEX tvItem;
						tvItem.mask = TVIF_PARAM;
						WCHAR* filePath;
						filePath = new WCHAR[10240];
						tvItem.hItem = hParent;
						tvItem.lParam = (LPARAM)filePath;
						TreeView_GetItem(treeView, &tvItem);
						LoadForderContent((WCHAR*)tvItem.lParam, &hParent);
					}
				}
				else
				{
					if (TreeView_GetRoot(treeView) == TreeView_GetSelection(treeView))
					{
						ClearListView();
						InitDriveColumn(listView);
						LoadDriveForListView();
					}
					else
					{
						ClearListView();
						InitFolderColumn(listView);
						HTREEITEM hParent = TreeView_GetSelection(treeView);
						TVITEMEX tvItem;
						tvItem.mask = TVIF_PARAM;
						WCHAR* filePath;
						filePath = new WCHAR[10240];
						tvItem.hItem = hParent;
						tvItem.lParam = (LPARAM)filePath;
						TreeView_GetItem(treeView, &tvItem);
						LoadForderContent((WCHAR*)tvItem.lParam, &hParent);
					}
				}
				return FALSE;
			}
		}

		break;
		}
	}
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	INITCOMMONCONTROLSEX icex;

	// Ensure that the common control DLL is loaded. 
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);
	
	// Get system font
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(lf.lfHeight, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);

	listView = CreateListView(hWnd);
	SendMessage(listView, WM_SETFONT, (WPARAM)hFont, TRUE);

	treeView = CreateTreeView(hWnd);
	SendMessage(treeView, WM_SETFONT, (WPARAM)hFont, TRUE);

	InitFolderColumn(listView);

	LoadThisPC();

	return TRUE;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch (id)
	{
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd)
{
	PostQuitMessage(0);
}

// Source: https://msdn.microsoft.com/en-us/library/windows/desktop/hh298360(v=vs.85).aspx
HWND CreateListView(HWND hWnd)
{
	RECT rcClient;                       // The parent window's client area.

	GetClientRect(hWnd, &rcClient);

	// Create the list-view window in report view with label editing enabled.
	HWND hWndListView = CreateWindow(WC_LISTVIEWW,
		L"",
		WS_CHILD | WS_VISIBLE | WS_BORDER | LVS_REPORT | LVS_EDITLABELS | WS_VSCROLL,
		rcClient.right / 3, 0, rcClient.right * 2 / 3, rcClient.bottom,
		hWnd,
		(HMENU)IDC_LISTVIEW,
		hInst,
		NULL);

	return (hWndListView);

}


// Source: https://msdn.microsoft.com/en-us/library/windows/desktop/hh298371(v=vs.85).aspx
HWND CreateTreeView(HWND hWnd)
{
	RECT rcClient;  // dimensions of client area 
	HWND hwndTV;    // handle to tree-view control 


	// Get the dimensions of the parent window's client area, and create 
	// the tree-view control. 
	GetClientRect(hWnd, &rcClient);
	hwndTV = CreateWindow(WC_TREEVIEW,
		L"",
		WS_VISIBLE | WS_CHILD | WS_BORDER | TVS_HASLINES | WS_VSCROLL |
		TVS_HASLINES | TVS_HASBUTTONS | TVS_LINESATROOT,
		0,
		0,
		rcClient.right / 3,
		rcClient.bottom,
		hWnd,
		(HMENU)IDC_TREEVIEW,
		hInst,
		NULL);

	return hwndTV;
}

void InitFolderColumn(HWND hwndListView)
{
	LVCOLUMN lvColumn;

	lvColumn.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;

	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 200;
	lvColumn.pszText = L"Name";
	ListView_InsertColumn(hwndListView, 0, &lvColumn);

	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 150;
	lvColumn.pszText = L"Type";
	ListView_InsertColumn(hwndListView, 1, &lvColumn);

	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.pszText = L"Date modified";
	ListView_InsertColumn(hwndListView, 2, &lvColumn);

	lvColumn.pszText = L"Size";
	ListView_InsertColumn(hwndListView, 3, &lvColumn);
}

void InitDriveColumn(HWND hwndListView)
{
	LVCOLUMN lvColumn;

	lvColumn.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;

	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 200;
	lvColumn.pszText = L"Name";
	ListView_InsertColumn(hwndListView, 0, &lvColumn);

	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 150;
	lvColumn.pszText = L"Capacity";
	ListView_InsertColumn(hwndListView, 1, &lvColumn);

	lvColumn.pszText = L"Free space";
	ListView_InsertColumn(hwndListView, 2, &lvColumn);

	lvColumn.pszText = L"Type";
	ListView_InsertColumn(hwndListView, 3, &lvColumn);
}

void LoadThisPC()
{
	TV_INSERTSTRUCT tvInsert;

	tvInsert.hParent = NULL;
	tvInsert.hInsertAfter = TVI_ROOT;
	tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM | TVIF_CHILDREN;
	tvInsert.item.pszText = _T("This PC");
	tvInsert.item.lParam = (LPARAM)_T("ThisPC");
	tvInsert.item.cChildren = 1;
	hRoot = TreeView_InsertItem(treeView, &tvInsert);
}

void GetDrive()
{
	TCHAR* logicalDriveString;
	logicalDriveString = new TCHAR[105];
	GetLogicalDriveStrings(105, logicalDriveString);

	logicalDriveList.clear();

	int i = 0;
	while (!(logicalDriveString[i] == 0 && logicalDriveString[i+1] == 0))
	{
		if (logicalDriveString[i] >= _T('A') && logicalDriveString[i] <= _T('Z'))
			logicalDriveList.push_back(logicalDriveString[i]);
		i++;
	}

	delete[] logicalDriveString;

	WCHAR* buffer;

	for (i = 0; i < logicalDriveList.size(); i++)
	{
		buffer = new WCHAR[5];
		wsprintf(buffer, L"%c:\\", logicalDriveList[i]);
		TV_INSERTSTRUCT tvInsert;
		tvInsert.hParent = hRoot;
		tvInsert.hInsertAfter = TVI_LAST;
		tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM | TVIF_CHILDREN;
		tvInsert.item.pszText = buffer;
		tvInsert.item.lParam = (LPARAM)buffer;
		tvInsert.item.cChildren = 1;
		TreeView_InsertItem(treeView, &tvInsert);
	}
}

void LoadDriveForListView()
{
	WCHAR *buffer;
	for (int i = 0; i < logicalDriveList.size(); i++)
	{
		buffer = new WCHAR[5];
		wsprintf(buffer, L"%c:\\", logicalDriveList[i]);
		LVITEM item;
		item.mask = LVIF_TEXT | LVIF_PARAM;
		item.iItem = i;
		item.iSubItem = 0;
		item.pszText = buffer;
		item.lParam = (LPARAM)buffer;
		ListView_InsertItem(listView, &item);

		UINT driveType = GetDriveType(buffer);

		if (driveType == DRIVE_FIXED || driveType == DRIVE_REMOVABLE)
		{
			__int64 totalSize, freeSpace;
			GetDiskFreeSpaceEx(buffer, NULL, (PULARGE_INTEGER)&totalSize, (PULARGE_INTEGER)&freeSpace);
			ListView_SetItemText(listView, i, 1, GetSize(totalSize));
			ListView_SetItemText(listView, i, 2, GetSize(freeSpace));
		}
		
		WCHAR type[50];
		if (driveType == DRIVE_FIXED)
			wsprintf(type, L"Local Disk");
		else
		{
			if (driveType == DRIVE_REMOVABLE)
				wsprintf(type, L"Removable Disk");
			else
			{
				if (driveType == DRIVE_CDROM)
					wsprintf(type, L"CD - Rom");
				else
					wsprintf(type, L"Unknown");
			}
		}
		ListView_SetItemText(listView, i, 3, type);
	}
}

void ClearListView()
{
	ListView_DeleteAllItems(listView);

	//Source: https://stackoverflow.com/questions/33281164/how-to-get-the-number-of-columns-in-a-list-control
	HWND hWndHdr = (HWND)::SendMessage(listView, LVM_GETHEADER, 0, 0);
	int count = (int)::SendMessage(hWndHdr, HDM_GETITEMCOUNT, 0, 0L);

	for (int i = 0; i < count; i++)
	{
		ListView_DeleteColumn(listView, 0);
	}
}

LPWSTR GetSize(__int64 size)
{
	int type = 0;
	while (size > 1024)
	{
		size /= 1024;
		type++;
	}

	WCHAR* buffer = new WCHAR[10];

	switch (type)
	{
	case 0:
		wsprintf(buffer, L"%d B", size);
		break;
	case 1:
		wsprintf(buffer, L"%d KB", size);
		break;
	case 2:
		wsprintf(buffer, L"%d MB", size);
		break;
	case 3:
		wsprintf(buffer, L"%d GB", size);
		break;
	default:
		buffer[0] = 0;
		break;
	}
	return buffer;
}

void LoadForderContent(LPCWSTR path, HTREEITEM* hParent)
{
	bool updateTreeFlag = false;
	bool hasParent = false;

	if (hParent != NULL)
		hasParent = true;
	if (TreeView_GetChild(treeView, TreeView_GetSelection(treeView)) == NULL && hasParent)
		updateTreeFlag = true;

	// Current folder path
	WCHAR buffer[10240];
	wcscpy_s(buffer, path);
	wcscat_s(buffer, L"*");

	WIN32_FIND_DATA findData;
	BOOL bFound = TRUE;
	HANDLE hFile = FindFirstFileW(buffer, &findData);
	// File path
	WCHAR *filePath;
	int count = 0;

	if (hFile == INVALID_HANDLE_VALUE)
		bFound = FALSE;

	while (bFound)
	{
		filePath = new WCHAR[10240];
		if ((findData.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY) &&
			(wcscmp(findData.cFileName, _T(".")) != 0) && (wcscmp(findData.cFileName, _T("..")) != 0))
		{
			// If folder
			
			wcscpy_s(filePath, 10240, buffer);
			filePath[wcslen(filePath) - 1] = 0;
			wcscat_s(filePath, 10240, findData.cFileName);
			wcscat_s(filePath, 10240, L"\\");

			LVITEM lvItem;
			lvItem.mask = LVIF_TEXT | LVIF_PARAM;
			lvItem.iItem = count;
			lvItem.iSubItem = 0;
			lvItem.pszText = findData.cFileName;
			lvItem.lParam = (LPARAM)filePath;
			ListView_InsertItem(listView, &lvItem);

			ListView_SetItemText(listView, count, 1, L"Folder");

			if (updateTreeFlag)
			{
				TVINSERTSTRUCT tvItem;
				tvItem.hParent = *hParent;
				tvItem.hInsertAfter = TVI_LAST;
				tvItem.item.mask = TVIF_TEXT | TVIF_PARAM | TVIF_CHILDREN;
				tvItem.item.pszText = findData.cFileName;
				tvItem.item.lParam = (LPARAM)filePath;
				tvItem.item.cChildren = 1;
				TreeView_InsertItem(treeView, &tvItem);
			}

			count++;
		}
		else
		{
			if ((findData.dwFileAttributes != FILE_ATTRIBUTE_DIRECTORY) &&
				(findData.dwFileAttributes != FILE_ATTRIBUTE_SYSTEM))
			{
				// If file
				wcscpy_s(filePath, 10240, buffer);
				filePath[wcslen(filePath) - 1] = 0;
				wcscat_s(filePath, 10240, findData.cFileName);

				LVITEM lvItem;
				lvItem.mask = LVIF_TEXT | LVIF_PARAM;
				lvItem.iItem = count;
				lvItem.iSubItem = 0;
				lvItem.pszText = findData.cFileName;
				lvItem.lParam = (LPARAM)filePath;
				ListView_InsertItem(listView, &lvItem);
				ListView_SetItemText(listView, count, 2, GetDateModified(findData.ftLastWriteTime));
				ListView_SetItemText(listView, count, 3, GetSize(findData.nFileSizeLow));

				delete[] filePath;
				count++;
			}
		}
		bFound = FindNextFileW(hFile, &findData);
		
	}
}

LPWSTR GetDateModified(const FILETIME &ftLastWrite)
{

	//Chuyển đổi sang local time
	SYSTEMTIME stUTC, stLocal;
	FileTimeToSystemTime(&ftLastWrite, &stUTC);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	TCHAR *buffer = new TCHAR[50];
	wsprintf(buffer, _T("%02d/%02d/%04d %02d:%02d"),
		stLocal.wDay, stLocal.wMonth, stLocal.wYear,
		stLocal.wHour,
		stLocal.wMinute);

	return buffer;
}