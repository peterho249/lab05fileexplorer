# README #

## Information ##
* ID: 1512077
* Name: Ho Xuan Dung

## Configuration System ##
* OS: Windows 10 x64
* IDE: Visual Studio 2017

## Function ##
* Create a list view and tree view.
* Show folders tree on tree view control.
* Show folders and files in a folder on list view control.

## Application Flow ##
### Main Flow ###
* Click on folder name on tree view, folders and files will be shown on list view.
* Double click on folder on list view, that folder will be opened.
* Double click on folder on tree view or press collapse button, subfolder will be shown below that folder.

### Addiction Flow ###
* Cannot double click a file to open.

## BitBucket Link ##
* (https://peterho249@bitbucket.org/peterho249/lab05fileexplorer.git)

## Youtube Link ##
* (https://youtu.be/ozpM-n277QE)